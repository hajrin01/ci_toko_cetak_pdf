-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Apr 2019 pada 13.00
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('AS09', 'PENSIL', 6000, 'GH03', 1, 19),
('BR001', 'pulpen', 8000, 'GH01', 1, 24),
('BR003', 'PC', 900000, 'GH01', 1, 8),
('BR004', 'Pulpen', 100000, 'GH03', 1, 10),
('JP001', 'Spidol', 8000, 'KH007', 1, 5),
('JP002', 'Buku', 8000, 'GH03', 1, 9),
('JP003', 'Pensil', 10000, 'GH03', 1, 4),
('JP004', 'Spidol', 8000, 'GH03', 1, 1),
('JP010', 'Pc', 5000000, 'GH02', 1, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('BR001', 'Manager Pasar', 'Operasional', 1),
('BR003', 'Marketing', 'Operasional', 1),
('JB00', 'Direktur', 'Manager', 1),
('JB000', 'Direktur', 'Manager', 1),
('JB002', 'DIREKTUR', 'OPERASIONAL', 1),
('JB003', 'Direktur', 'Operasional', 1),
('JB005', 'Direktur', 'Operasional', 1),
('JB009', 'Direktur', 'Manager', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('', 'Alat Tulis kantor', 1),
('GH01', 'Alat Tulis tulis', 1),
('GH02', 'Perangkat lunak', 1),
('GH03', 'Alat Tulis kantor', 1),
('SD09', 'Alat  cukur', 1),
('KH009', 'Alat Tulis kantor', 1),
('KH008', 'Perangkat lunak', 1),
('KH007', 'Alat Tulis kantor', 1),
('KH003', 'Alat Tulis kantor', 1),
('KH002', 'Alat Tulis kantor', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `photo`) VALUES
('1704421312', 'Furkan', 'Kampung Baru', '2000-12-17', 'L', 'Jl. Ahmad Yani', '082359150688', 'BR003', 1, '20190414_1704421312.jpg'),
('1704421315', 'Muhrin ', 'Ende', '0000-00-00', 'p', '      yjtyjty', '082359150340', 'BR001', 1, 'default.jpg'),
('1704421317', 'Muhrin Hajrin', 'Kampung Baru', '0000-00-00', 'L', 'fbfdbf', '082359150688', 'BR001', 1, 'default.jpg'),
('170442133', 'Muhrin Hajrin', 'Ende', '0000-00-00', 'L', 'dvddf', '082359150340', 'JB005', 1, 'default.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(18, 20, 'BR002', 2, 5000000, 10000000, 1),
(20, 21, 'BR001', 2, 12000000, 24000000, 1),
(21, 21, 'BR003', 3, 15000000, 45000000, 1),
(22, 21, 'BR003', 6, 80000, 480000, 1),
(23, 21, 'BR004', 4, 80000, 320000, 1),
(24, 21, 'AS09', 10, 600, 6000, 1),
(25, 21, 'BR004', 7, 20000, 140000, 1),
(26, 22, 'BR001', 7, 500000, 3500000, 1),
(27, 22, 'BR003', 7, 20000000, 140000000, 1),
(28, 22, 'BR003', 7, 20000000, 140000000, 1),
(29, 34, 'BR003', 12, 50000000, 600000000, 1),
(30, 35, 'BR001', 9, 200000, 1800000, 1),
(31, 36, 'JP010', 7, 20000000, 140000000, 1),
(32, 32, 'AS09', 7, 200000, 1400000, 1),
(33, 32, 'JP001', 4, 200000, 800000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` int(11) NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `kode_supplier`, `tanggal`, `approved`, `flag`) VALUES
(21, 19002, 'PG002', '2019-03-23', 1, 1),
(22, 19003, 'PG003', '2019-03-23', 1, 1),
(23, 19004, 'PG002', '2019-03-23', 1, 1),
(26, 19005, 'PG001', '2019-03-23', 1, 1),
(27, 19006, 'PG002', '2019-03-23', 1, 1),
(32, 3838, 'PG001', '2019-03-26', 1, 1),
(33, 19004, 'DT002', '2019-04-11', 1, 1),
(34, 12464, 'PG002', '2019-04-11', 1, 1),
(35, 12056, 'DT002', '2019-04-11', 1, 1),
(36, 12057, 'DT002', '2019-04-15', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_jual_d`, `id_jual_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 12346, 'AS09', 12, 20000000, 20000000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `pembeli` varchar(250) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan_header`
--

INSERT INTO `penjualan_header` (`id_jual_h`, `no_transaksi`, `tanggal`, `pembeli`, `flag`) VALUES
(12345, 19002, '2019-04-01', 'Muhrin', 1),
(12346, 12057, '2019-04-13', 'Fara', 1),
(12347, 12464, '2019-04-14', 'Fara', 1),
(12348, 12057, '2019-04-15', 'Fara', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('DT002', 'Rahmat', 'Jl. Warakas', '08965444229', 1),
('PG001', 'RAIS', 'Jl.sungai bambu', '0218838383', 1),
('PG002', 'Furkan', 'Jl.Plumpang No 17', '089922223333', 1),
('PG003', 'Ahmad', 'Jl.Rawabadak Timur No.17', '089621650023', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, 'Admin', 'muhrinhajrin02@gmail.com', 'c93ccd78b2076528346216b3b2f701e6', 1, 1),
(2, '1704421300', 'rinhajrin0@gmail.com', 'cc63027830c1a0db0c6d6b69e91dde3b', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12349;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
