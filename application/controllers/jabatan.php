<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jabatan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	
	}
	
	public function index()
	
	{
		$this->listJabatan();
		
	}
	
	public function listJabatan()
	
	{	
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_jabatan'] = $this->jabatan_model->tombolpagination($data['kata_pencarian']);


		//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']       ='forms/list_jabatan';
		$this->load->view('home_2', $data);
	}
	public function inputjabatan()
	
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		//if (!empty($_REQUEST)) {
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->save();
			//redirect("jabatan/index", "refresh");
			$validation = $this->form_validation;
			$validation->set_rules($this->jabatan_model->rules());
			
			if ($validation->run()){
				$this->jabatan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("jabatan/index", "refresh");
			
		}
			
		//$this->load->view('input_jabatan', $data);
		$data['content']       ='forms/input_jabatan';
		$this->load->view('home_2', $data);
		
	}
	public function detailjabatan($kode_jabatan)
	
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content']       ='forms/detail_jabatan';
		$this->load->view('home_2', $data);
	}
	
	public function editjabatan($kode_jabatan)
	
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['detail_jabatan'] = $this->jabatan_model->detail ($kode_jabatan);
			//if (!empty($_REQUEST)) {
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->update($kode_jabatan);
			//redirect("jabatan/index", "refresh");

		$validation = $this->form_validation;
			$validation->set_rules($this->jabatan_model->rules());
			
			if ($validation->run()){
				$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("jabatan/index", "refresh");
		}
		
			
		$data['content']       ='forms/edit_jabatan';
		$this->load->view('home_2', $data);
	}
	
	public function deletejabatan($kode_jabatan)
	{
		$m_karyawan = $this->jabatan_model;
		$m_karyawan->delete($kode_jabatan);
		redirect("jabatan/index", "refresh");
	}
	
}
