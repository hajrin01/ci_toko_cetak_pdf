<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tampilhome extends CI_Controller {
	
	public function index()
	{
		$this->listhome();
	}
	
	public function listhome()
	{
		$this->load->view('home');
	}
	
}