<?php defined('BASEPATH') OR exit('No direct script acces allowed');

Class Penjualan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("penjualan_model");
		// $this->load->model("supplier_model");
		$this->load->model("barang_model");

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	// $this->load->library("pdf/pdf");
	}
	
	
	public function index()
	{
		$this->listPenjualan();
    }
	
	
	public function listPenjualan()

	{	
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_penjualan'] = $this->penjualan_model->tombolpagination($data['kata_pencarian']);

       
		//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']       ='forms/list_penjualan';
		$this->load->view('home_2', $data);
	


	// {
	// 	$data['data_penjualan'] = $this->penjualan_model->tampilDataPenjualan();
 //        $data['content']       ='forms/list_penjualan';
	// 	$this->load->view('home_2', $data);
 //    }
    
	}
	
	public function inputjual()
	
	{
      
        $data['content']  ='forms/input_penjualan_header';		
		$validation = $this->form_validation;
		$validation->set_rules($this->penjualan_model->rules());
			
			if ($validation->run()){
			
				if (!empty($_REQUEST)) {
           		$penjualan_header = $this->penjualan_model;
           		$penjualan_header->savePenjualanHeader();
         
            //panggil ID transaksi terakhir
            	$id_terakhir = $penjualan_header->idTransaksiTerakhir();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("penjualan/inputDetail/" . $id_terakhir, "refresh");
        }

    }
        

		$this->load->view('home_2', $data);
    }
	
	
	public function inputDetail($id_penjualan_header)


	{
        // panggil data barang untuk kebutuhan form input
		
         $data['data_barang'] = $this->barang_model->tampilDataBarang();
         $data['id_header'] = $id_penjualan_header;
         $data['data_penjualan_detail'] = $this->penjualan_model->tampilDataPenjualanDetail($id_penjualan_header);
     	$validation = $this->form_validation;
		$validation->set_rules($this->penjualan_model->rules1());
			
			if ($validation->run()){

		   if (!empty($_REQUEST)) {
            //save detail
            $this->penjualan_model->savePenjualanDetail($id_penjualan_header);
            
            //proses update stok
            $kode_barang  = $this->input->post('kode_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStok($kode_barang, $qty);
			$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("penjualan/inputDetail/" . $id_penjualan_header, "refresh");
        }
    }
        
		$data['content']       ='forms/input_penjualan_detail';
		$this->load->view('home_2', $data);
	}

	// public function dtp()
	// {
	// 	//$this->load->view('list_laporan_pembelian');
	// 	$data['content']       ='forms/list_dtp';
	// 	$this->load->view('home_2', $data);
	// }

	// public function laporanPembelian()
	// {    
	// 		// echo "<prev>";
	// 	 // print_r($this->input->post('tgl_awal'));die();
	// 	 //   echo "</prev>";

	// 	 $tgl_awal=$this->input->post('tgl_awal');
	// 	 $tgl_akhir=$this->input->post('tgl_akhir');
	// 	 $data['data_pembelian_detail'] = $this->pembelian_model->tampilLaporanPembelian2($tgl_awal, $tgl_akhir);
	// 	$data['tgl_awal'] = $tgl_awal;
	// 	$data['tgl_akhir']= $tgl_akhir;

	// 	$data['content'] ='forms/list_laporan_pembelian';
	// 	$this->load->view('home_2', $data);
	// }


// public function cetak($tgl_awal, $tgl_akhir)
//    {
//         $pdf = new FPDF('P','mm','A4');
//         // membuat halaman baru
//         $pdf->AddPage();
//         // setting jenis font yang akan digunakan
//         $pdf->SetFont('Arial', 'B' ,15);
//         // mencetak string 
//         $pdf->Cell(187, 7, 'LAPORAN DATA PEMBELIAN', 0, 1, 'C');
//         $pdf->SetFont('Arial','B',15);
//         $pdf->Cell(190,7,'TOKO JAYA ABADI',0,1,'C');
//         // Memberikan space kebawah agar tidak terlalu rapat
//        $pdf->Cell(10,10,'',0,1,'L');
//         $pdf->SetFont('Arial','B',10);
//         $pdf->Cell(18, 6, 'No', 1, 0, 'C');
//          $pdf->Cell(22, 6, 'Id pembelian', 1, 0, 'C');
//         $pdf->Cell(35, 6, 'Nomor transaksi', 1, 0, 'C');
//         $pdf->Cell(30,6,'Tanggal',1,0,'C');
       
//         $pdf->Cell(30,6,'Total barang',1,0,'C');
//         $pdf->Cell(30,6,'Total qty',1,0,'C');
//         $pdf->Cell(29,6,'Jumlah nominal',1,1,'C');
         
//         $pdf->SetFont('Arial','B',10);
//         $no = 0;
//         $total_keseluruhan = 0;
//         $data_pembelian_detail = $this->pembelian_model->tampilLaporanPembelian2($tgl_awal,$tgl_akhir);
       
//         foreach ($data_pembelian_detail as $data){
//             $no ++;
//              $pdf->Cell(18,6,$no,1,0,'C');
//               $pdf->Cell(22,6,$data->id_pembelian_h,1,0,'C');
//             $pdf->Cell(35,6,$data->no_transaksi,1,0,'C');
//             $pdf->Cell(30,6,$data->tanggal,1,0,'C');
//             $pdf->Cell(30,6,$data->total_barang,1,0,'C'); 
//             $pdf->Cell(30,6,$data->total_qty,1,0,'C'); 
//             $pdf->Cell(29,6,'Rp.'. number_format($data->total_pembelian),1,1,'R');

//               $total_keseluruhan += $data->total_pembelian; 
//         }
//         $pdf->SetFont('Arial','B',10);
//         $pdf->Cell(165,6,'Total Keseluruhan',1,0,'C');
//         $pdf->Cell(29,6,'Rp.'. number_format($total_keseluruhan),1,1,'R');
//         $pdf->Output();
//     }
 
}
