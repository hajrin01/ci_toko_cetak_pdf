<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawan extends CI_controller {


	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("Karyawan_model");
		$this->load->model("jabatan_model");

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	
	}
	
	public function index()
	
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	
	{
		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_karyawan'] = $this->Karyawan_model->tombolpagination($data['kata_pencarian']);
 		
		//$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['content']       ='forms/list_karyawan';
		$this->load->view('home_2', $data);
	}
	public function inputkaryawan()
	
	{
		$nik = $this->input->POST('nik');
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content'] = 'forms/input_karyawan';
		$validation = $this->form_validation;
			$validation->set_rules($this->Karyawan_model->rulesinput());
			
			if ($validation->run()){
					//echo "<pre>";
		//print_r($_FILES["image"]); die();
		//echo "</pre>";

				$this->Karyawan_model->save();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("karyawan/index", "refresh");
		}
			
		
			$this->load->view('home_2', $data);
	}
	public function detailKaryawan($nik)
	
	{
		$data['detail_karyawan'] = $this->Karyawan_model->detail($nik);
		$data['content']       ='forms/detail_karyawan';
		$this->load->view('home_2', $data);
					
	}
	public function editkaryawan($nik)
	
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['detail_karyawan'] = $this->Karyawan_model->detail ($nik) ;
		$data['content']       ='forms/edit_karyawan';
			
		$validation = $this->form_validation;
		$validation->set_rules($this->Karyawan_model->rules());
			
			if ($validation->run()){
				$this->Karyawan_model->update($nik);
				$this->session->set_flashdata('info', '<div style="color: green">EDIT DATA BERHASIL! </div>');
				redirect("karyawan/index", "refresh");
	}	
			$this->load->view('home_2', $data);
			
	}


	public function deletekaryawan($nik)
	{
		$m_karyawan = $this->Karyawan_model;
		$m_karyawan->delete($nik);
		redirect("karyawan/index", "refresh");
	}
}
