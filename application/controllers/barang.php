<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Barang extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");


		//load validasi
		$this->load->library("form_validation");


		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	
	}
	
	public function index()
	
	{
		$this->listBarang();
	}
	
	public function listBarang()
	
	{

		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['data_barang'] = $this->barang_model->tombolpagination($data['kata_pencarian']);

		//$data['data_barang'] = $this->barang_model->tampilDataBarang2();
		$data['content']       ='forms/list_barang';
		$this->load->view('home_2', $data);
	}
	
	public function inputbarang()
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		
		
		//if (!empty($_REQUEST)) {
			//$m_barang = $this->barang_model;
			//$m_barang->save();
			//redirect("barang/index", "refresh");
			
			//validasi terlebih dahulu
			$validation = $this->form_validation;
			$validation->set_rules($this->barang_model->rules());
			
			if ($validation->run()){
				$this->barang_model->save();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("barang/index", "refresh");
				}
			
			
			$data['content'] = 'forms/input_barang';
			$this->load->view('home_2', $data);
		
		}
	
	
	public function detailbarang($kode_barang)
	
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']       ='forms/detail_barang';
		$this->load->view('home_2', $data);
	}
	
	
	public function editbarang($kode_barang)
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['detail_barang'] = $this->barang_model->detail ($kode_barang);
			//if (!empty($_REQUEST)) {
			//$m_barang = $this->barang_model;
			//$m_barang->update($kode_barang);
			//redirect("barang/index", "refresh");
		$validation = $this->form_validation;
			$validation->set_rules($this->barang_model->rules());
			
		if ($validation->run()){
			$this->barang_model->update($kode_barang);
		   $this->session->set_flashdata('info', '<div style="color: green">EDIT DATA BERHASIL! </div>');
				redirect("barang/index", "refresh");

		}
		
		//$this->load->view('edit_barang', $data);
		$data['content']       ='forms/edit_barang';
		$this->load->view('home_2', $data);
	}
	
	public function deletebarang($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index", "refresh");
	}
	
}
