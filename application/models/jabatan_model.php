<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class jabatan_model extends CI_Model
{
	//panggil nama table
	private $_table = "jabatan";


public function rules()
{
		return[
		[
				'field' =>'kode_jabatan',
				'label' =>'Kode Jabatan',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'kode jabatan tidak boleh kosong.',
					'max_length' => 'kode jabatan tidak boleh lebih dari 5 karakter.',
				]

		],
		[
				'field' =>'nama_jabatan',
				'label' =>'Nama jabatan',
				'rules' =>'required',
				'errors' =>[
					'required' => 'nama jabatan tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'keterangan',
				'label' =>'Keterangan',
				'rules' =>'required',
				'errors' =>[
					'required' => 'Keterangan tidak boleh kosong.',
					
		
		
		]
		]
];
}
	
	
	public function tampilDataJabatan()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataJabatan2()
	
	{
		$query = $this->db->query("SELECT * FROM jabatan WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDataJabatan3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_jabatan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	
	{
		
		
		$data['kode_jabatan'] =$this->input->post('kode_jabatan');
		$data['nama_jabatan'] =$this->input->post('nama_jabatan');
		$data['keterangan'] =$this->input->post('keterangan');
		
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);




	}
	public function detail($kode_jabatan)
	
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	
	public function update($kode_jabatan)
	
	{
		$data['kode_jabatan'] =$this->input->post('kode_jabatan');
		$data['nama_jabatan'] =$this->input->post('nama_jabatan');
		$data['keterangan'] =$this->input->post('keterangan');
		$data['flag'] =1;
		
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->update($this->_table, $data);

	}
	
	public function delete($kode_jabatan)
	{
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->delete($this->_table);	
	}


	public function tampilDataJabatanPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('nama_jabatan', $data_pencarian);
		}
		$this->db->order_by('kode_jabatan','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
}


public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_jabatan', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'jabatan/listjabatan/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="pagination">';
	$pagination['full_tag_close'] = '</div>';
	
	$pagination['first_link'] = '&nbsp;&nbsp;<button type="button" 
								class="btn btn-info">First</button>';
	$pagination['first_tag_open'] = '<span class="firstlink">';
	$pagination['first_tag_close'] = '</span>&nbsp;<&nbsp;';
	
	
	$pagination['last_link'] = '&nbsp;&nbsp;>&nbsp;<button type="button" 
								class="btn btn-info">Last</button>';
	$pagination['last_tag_open'] = '<span class="lastlink">';
	$pagination['last_tag_close'] = '</span>';
	
	$pagination['next_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
								Next</button>';
	$pagination['next_tag_open'] = '<span class="nextlink">';
	$pagination['next_tag_close'] = '</span>';
	
	
	$pagination['prev_link'] = '<button type="button" class="btn btn-info">
								Prev</button>';
	$pagination['prev_tag_open'] = '<span class="prevlink">';
	$pagination['prev_tag_close'] = '</span>&nbsp;&nbsp;';
	
	
	$pagination['cur_tag_open'] = '<span class="curlink" style="color: red">';
	$pagination['cur_tag_close'] = '</span>&nbsp;&nbsp;';
	
	$pagination['num_tag_open'] = '<span class="numlink">';
	$pagination['num_tag_close'] = '&nbsp;&nbsp;</span>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataJabatanPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}


}
