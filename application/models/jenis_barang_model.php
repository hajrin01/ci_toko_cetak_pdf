<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class jenis_barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "jenis_barang";

public function rules()
{
		return[
		[
				'field' =>'kode_jenis',
				'label' =>'Kode jenis',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'kode jenis tidak boleh kosong.',
					'max_length' => 'kode jenis tidak boleh lebih dari 5 karakter.',
				]

		],
		[
				'field' =>'nama_jenis',
				'label' =>'Nama jenis',
				'rules' =>'required',
				'errors' =>[
					'required' => 'nama jenis tidak boleh kosong.',
					
		]
	    ]
];
}
	

	
	public function tampilDatajenisBarang()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDatajenisBarang2()
	
	{
		$query = $this->db->query("SELECT * FROM jenis_barang WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDatajenisBarang3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_jenis', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	
	{
		
		
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['nama_jenis'] =$this->input->post('nama_jenis');
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);

	}

	public function detail($kode_jenis)
	
	{
		$this->db->select('*');
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	public function update($kode_jenis)
	{
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['nama_jenis'] =$this->input->post('nama_jenis');
		$data['flag'] =1;

		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->update($this->_table, $data);
	}

	public function delete($kode_jenis)
	{
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->delete($this->_table);	
	}



  public function tampilDataJenisBarangPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('nama_jenis', $data_pencarian);
		}
		$this->db->order_by('kode_jenis','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
	}



	public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_jenis', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'jenis_barang/listjenisbarang/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="pagination">';
	$pagination['full_tag_close'] = '</div>';
	
	$pagination['first_link'] = '&nbsp;&nbsp;<button type="button" 
								class="btn btn-info">First</button>';
	$pagination['first_tag_open'] = '<span class="firstlink">';
	$pagination['first_tag_close'] = '</span>&nbsp;<&nbsp;';
	
	
	$pagination['last_link'] = '&nbsp;&nbsp;>&nbsp;<button type="button" 
								class="btn btn-info">Last</button>';
	$pagination['last_tag_open'] = '<span class="lastlink">';
	$pagination['last_tag_close'] = '</span>';
	
	$pagination['next_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
								Next</button>';
	$pagination['next_tag_open'] = '<span class="nextlink">';
	$pagination['next_tag_close'] = '</span>';
	
	
	$pagination['prev_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
								Prev</button>';
	$pagination['prev_tag_open'] = '<span class="prevlink">';
	$pagination['prev_tag_close'] = '</span>&nbsp;&nbsp;';
	
	
	$pagination['cur_tag_open'] = '<span class="curlink" style="color: red">';
	$pagination['cur_tag_close'] = '</span>&nbsp;&nbsp;';
	
	$pagination['num_tag_open'] = '<span class="numlink">';
	$pagination['num_tag_close'] = '&nbsp;&nbsp;</span>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataJenisBarangPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}


}


