<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";
	
public function rulesinput()
	
	{
		return [
		[
		'field' => 'nik',
		'label' => 'Nik',
		'rules' => 'trim|required|max_length[20]|is_unique[karyawan.nik]',
		'errors' => [
		   'required' => 'Nik tidak boleh Kosong.',
		   'max_length' => 'Nik tidak boleh lebih dari 20 karakter.',
		   'is_unique'	=> 'Nik tidak boleh sama',
		   ],
		   ],
		   [
		 'field' => 'nama_lengkap',
		 'label' => 'Nama Lengkap',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'Nama tidak boleh kosong.',
           		   ],
		   ],
		   [
         'field' => 'tempat_lahir',
		'label' => 'Tempat Lahir',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Tempat Lahir tidak boleh kosong.',

           		   ],
		   ],
		   [
		'field' => 'tgl_lahir',
		'label' => 'Tanggal Bulan Tahun',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Tanggal Bulan Tahun tidak boleh kosong.',

           		   ],
		   ],
		   [
		 'field' => 'jenis_kelamin',
		'label' => 'Jenis Kelamin',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Jenis Kelamin tidak boleh kosong.',

           		   ],
           	],
            [
		'field' => 'alamat',
		'label' => 'Alamat',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Alamat tidak boleh kosong.',

           		   ],
		   ],
		    [
		'field' => 'telp',
		'label' => 'Telp',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'Telp tidak boleh kosong.',
		   'numeric'  =>'Telp Harus Angka.',

           		   ],
		   ],
		    [
		'field' => 'kode_jabatan',
		'label' => 'Kode Jabatan',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Kode Jabatan tidak boleh kosong.',

           ]
		   ]
		  		 ];
	}
	
public function rules()
	
	{
		return [
		[
		'field' => 'nik',
		'label' => 'Nik',
		'rules' => 'required|max_length[20]',
		'errors' => [
		   'required' => 'Nik tidak boleh Kosong.',
		   'max_length' => 'Nik tidak boleh lebih dari 20 karakter.',
		 	],
		   ],
		   [
		 'field' => 'nama_lengkap',
		 'label' => 'Nama Lengkap',
		 'rules' => 'required',
		 'errors' => [
		   'required' => 'Nama tidak boleh kosong.',
           		   ],
		   ],
		   [
         'field' => 'tempat_lahir',
		'label' => 'Tempat Lahir',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Tempat Lahir tidak boleh kosong.',

           		   ],
		   ],
		   [
		'field' => 'tgl_lahir',
		'label' => 'Tanggal Bulan Tahun',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Tanggal Bulan Tahun tidak boleh kosong.',

           		   ],
		   ],
		   [
		 'field' => 'jenis_kelamin',
		'label' => 'Jenis Kelamin',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Jenis Kelamin tidak boleh kosong.',

           		   ],
           	],
            [
		'field' => 'alamat',
		'label' => 'Alamat',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Alamat tidak boleh kosong.',

           		   ],
		   ],
		    [
		'field' => 'telp',
		'label' => 'Telp',
		'rules' => 'required|numeric',
		'errors' => [
		   'required' => 'Telp tidak boleh kosong.',
		   'numeric'  =>'Telp Harus Angka.',

           		   ],
		   ],
		    [
		'field' => 'kode_jabatan',
		'label' => 'Kode Jabatan',
		'rules' => 'required',
		'errors' => [
		   'required' => 'Kode Jabatan tidak boleh kosong.',

           ]
		   ]
		  		 ];
	}
	


	
	public function tampilDataKaryawan()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataKaryawan2()
	
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDataKaryawan3()
	
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	public function save()
	{
		$tgl_lahir = $this->input->post('tgl_lahir');

		$nik_karyawan = $this->input->POST('nik');
		$sql = $this->db->query("SELECT nik FROM karyawan where nik='$nik'");
		$cek_nik = $sql->num_rows();
			if ($cek_nik > 0)
	{

		$this->session->set_flashdata('nik');
		redirect('karyawan/inputkaryawan');

	}

	$nik_karyawan = $this->input->post('nik');

	
		$data['nik'] =$nik_karyawan;
		$data['nama_lengkap'] =$this->input->post('nama_lengkap');
		$data['tempat_lahir'] =$this->input->post('tempat_lahir');
		$data['tgl_lahir'] =$tgl_lahir;
		$data['jenis_kelamin'] =$this->input->post('jenis_kelamin');
		$data['alamat'] =$this->input->post('alamat');
		$data['telp'] =$this->input->post('telp');
		$data['kode_jabatan'] =$this->input->post('kode_jabatan');
		$data['flag'] =1;
		$data['photo'] =$this->uploadphoto($nik_karyawan);
		$this->db->insert($this->_table, $data);
	
}

	public function detail($nik)
	
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}



	public function update($nik)
	{
	
	$tgl_lahir = $this->input->post('tgl_lahir');
		
		echo "<prev>";
		 print_r($this->input->post('tgl_lahir'));die();
	echo "</prev>";
		$data['nama_lengkap'] =$this->input->post('nama_lengkap');
		$data['tempat_lahir'] =$this->input->post('tempat_lahir');
		$data['tgl_lahir'] =$this->input->post('tgl_lahir');
		$data['jenis_kelamin'] =$this->input->post('jenis_kelamin');
		$data['alamat'] =$this->input->post('alamat');
		$data['telp'] =$this->input->post('telp');
		$data['kode_jabatan'] =$this->input->post('kode_jabatan');
		$data['flag'] =1;
		
		if(!empty($_FILES["image"]["name"])) {
			$this->hapusphoto($nik);
			$foto_karyawan = $this->uploadphoto($nik);
		} else {
			$foto_karyawan = $this->input->post('foto_old');
		}
		$data['photo'] = $foto_karyawan;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);

	}


	public function delete($nik)
	
	{
		
		$this->db->where('nik', $nik);
	    $this->db->delete($this->_table);
		
	}

	
	
	private function uploadphoto($nik)
	{
		$date_upload  = date('Ymd');
		$config['upload_path'] = './resources/fotokaryawan/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] = $date_upload . '_' . $nik;
		$config['overwrite'] = true;
		$config['max_size'] = 1024; //1mb
		
		//echo "<pre>";
		//print_r($_FILES["image"]); die();
		//echo "</pre>";
		
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload('image')) {
			$nama_file = $this->upload->data("file_name");
		} else {
			$nama_file = "default.jpg";
		}
		return $nama_file;
	}
	
	
	private function hapusphoto($nik)
	{
		$data_karyawan  = $this->detail($nik);
		foreach ($data_karyawan as $data) {
			$nama_file = $data->photo;
		
		}
		if ($nama_file != "default.jpg") {
			$path = "./resources/fotokaryawan/" . $nama_file;
			// bentuk pathnya : ./resources/fotokaryawan/20190328_9876876.jpg
			return @unlink($path);
		}
	}	

public function tampilDataKaryawanPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('*');
	if (!empty($data_pencarian)) {
		$this->db->like('nama_lengkap', $data_pencarian);
		}
		$this->db->order_by('nik','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
}

	
	
	
	public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_lengkap', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'karyawan/listkaryawan/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="pagination">';
	$pagination['full_tag_close'] = '</div>';
	
	$pagination['first_link'] = '&nbsp;&nbsp;<button type="button" 
								class="btn btn-info">First</button>';
	$pagination['first_tag_open'] = '<span class="firstlink">';
	$pagination['first_tag_close'] = '</span>&nbsp;<&nbsp;';
	
	
	$pagination['last_link'] = '&nbsp;&nbsp;>&nbsp;<button type="button" 
								class="btn btn-info">Last</button>';
	$pagination['last_tag_open'] = '<span class="lastlink">';
	$pagination['last_tag_close'] = '</span>';
	
	$pagination['next_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
								Next</button>';
	$pagination['next_tag_open'] = '<span class="nextlink">';
	$pagination['next_tag_close'] = '</span>';
	
	
	$pagination['prev_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
								Prev</button>';
	$pagination['prev_tag_open'] = '<span class="prevlink">';
	$pagination['prev_tag_close'] = '</span>&nbsp;&nbsp;';
	
	
	$pagination['cur_tag_open'] = '<span class="curlink" style="color: red">';
	$pagination['cur_tag_close'] = '</span>&nbsp;&nbsp;';
	
	$pagination['num_tag_open'] = '<span class="numlink">';
	$pagination['num_tag_close'] = '&nbsp;&nbsp;</span>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataKaryawanPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}

}
