<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";

public function rules()
{
		return[
		[
				'field' =>'kode_barang',
				'label' =>'Kode Barang',
				'rules' =>'required|max_length[5]',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
				]

		],
		[
				'field' =>'nama_barang',
				'label' =>'Nama Barang',
				'rules' =>'required',
				'errors' =>[
					'required' => 'nama barang tidak boleh kosong.',
					
				]

		],
		[
				'field' =>'harga_barang',
				'label' =>'Harga Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'harga barang tidak boleh kosong.',
					'numeric' => 'harga barang harus angka.',
				]
		],
		[
				'field' =>'kode_jenis',
				'label' =>'Kode Jenis',
				'rules' =>'required',
				'errors' =>[
					'required' => 'kode barang tidak boleh kosong.',
					
				]
		],
		[
				'field' =>'stok',
				'label' =>'Stok Barang',
				'rules' =>'required|numeric',
				'errors' =>[
					'required' => 'stok barang tidak boleh kosong.',
					'numeric' => 'stok barang harus angka.',
		]
		]
];
}
	


	
	public function tampilDataBarang()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataBarang2()
	
	{
		$query = $this->db->query("select * from jenis_barang as jb inner join barang as br on jb.kode_jenis=br.kode_jenis");
		return $query->result();
	}
	
	public function tampilDataBarang3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()

	{
		
		$data['kode_barang'] =$this->input->post('kode_barang');
		$data['nama_barang'] =$this->input->post('nama_barang');
		$data['harga_barang'] =$this->input->post('harga_barang');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['stok'] =$this->input->post('stok');
		$data['flag'] =1;
		$this->db->insert($this->_table, $data);
		
		}
		
	public function detail($kode_barang)
	
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function update($kode_barang)
	
	{
		$data['kode_barang'] =$this->input->post('kode_barang');
		$data['nama_barang'] =$this->input->post('nama_barang');
		$data['harga_barang'] =$this->input->post('harga_barang');
		$data['kode_jenis'] =$this->input->post('kode_jenis');
		$data['stok'] =$this->input->post('stok');
		
		$data['flag'] =1;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);

	}
	
	public function delete($kode_barang)
	{
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);	
	}
	
	
	
	public function updateStok($kode_barang, $qty)
	{
		$cari_stok = $this->detail($kode_barang);
		foreach ($cari_stok as $data ){
			$stok = $data->stok;
	}
		
		$jumlah_stok = $stok + $qty;
		$data_barang['stok'] = $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}


public function tampilDataBarangPagination($perpage, $uri, $data_pencarian)
{
	$this->db->select('barang.kode_barang, barang.nama_barang, barang.harga_barang, jenis_barang.nama_jenis, barang.stok');
	$this->db->join('jenis_barang', 'jenis_barang.kode_jenis = barang.kode_jenis');
	if (!empty($data_pencarian)) {
		$this->db->like('barang.nama_barang', $data_pencarian);
		}
		$this->db->order_by('barang.kode_barang','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
			
			}else{
				return null;
			}	
	}

	public function tombolpagination($data_pencarian)
{
	$this->db->like('nama_barang', $data_pencarian);
	$this->db->from($this->_table);
	$hasil = $this->db->count_all_results();
	
	//pagination limt
	$pagination['base_url'] = base_url().'barang/listbarang/load/';
	$pagination['total_rows'] =$hasil;
	$pagination['per_page'] = "3";
	$pagination['uri_segment'] = 4;
	$pagination['num_links'] = 2;
	
	
	$pagination['full_tag_open'] = '<div class="pagination">';
	$pagination['full_tag_close'] = '</div>';
	
	$pagination['first_link'] = '&nbsp;&nbsp;<button type="button" 
								class="btn btn-info">First</button>';
	$pagination['first_tag_open'] = '<span class="firstlink">';
	$pagination['first_tag_close'] = '</span>&nbsp;<&nbsp;';
	
	
	$pagination['last_link'] = '&nbsp;&nbsp;>&nbsp;<button type="button" 
								class="btn btn-info">Last</button>';
	$pagination['last_tag_open'] = '<span class="lastlink">';
	$pagination['last_tag_close'] = '</span>';
	
	$pagination['next_link'] = '&nbsp;&nbsp;<button type="button" class="btn btn-info">
								Next</button>';
	$pagination['next_tag_open'] = '<span class="nextlink">';
	$pagination['next_tag_close'] = '</span>';
	
	
	$pagination['prev_link'] = '<button type="button" class="btn btn-info">
								Prev</button>';
	$pagination['prev_tag_open'] = '<span class="prevlink">';
	$pagination['prev_tag_close'] = '</span>&nbsp;&nbsp;';
	
	
	$pagination['cur_tag_open'] = '<span class="curlink" style="color: red">';
	$pagination['cur_tag_close'] = '&nbsp;&nbsp;</span>';
	
	$pagination['num_tag_open'] = '<span class="numlink">';
	$pagination['num_tag_close'] = '&nbsp;&nbsp;</span>';
	
	$this->pagination->initialize($pagination);
	
	$hasil_pagination = $this->tampilDataBarangPagination($pagination['per_page'],
	$this->uri->segment(4), $data_pencarian);
	
	return $hasil_pagination;
	
	}

}

