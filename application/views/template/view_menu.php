
<div style="background:#333;color:#fff;margin:5px 0;padding:10px 0"><h2 style="font-family:Georgia, 'Times New Roman', Times, serif">TOKO JAYA ABADI</h2></div>

 <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
 
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
<div class='bbt-menu'>
<div class='nav-menu'>
<nav>
  <label for='drop' class='toggle'>Menu <span>&#9776;</span></label>
  <input type='checkbox' id='drop' />
  <ul class='menu'>
     <li><a href="<?=base_url();?>Tampilhome/listhome" style="text-decoration:none;">Home</a></li>
   
    <li> 
      <!-- First Tier Drop Down -->
      <label for='drop-1' class='toggle'>Master <span>&#9776;</span></label>
      <a href='#'>Master &#9776;</a>
      <input type='checkbox' id='drop-1'/>
      <ul>
        
        <li><a href="<?=base_url();?>karyawan/listkaryawan" style="text-decoration:none;">Data Karyawan</a></li>
        <li><a href="<?=base_url();?>jabatan/listjabatan" style="text-decoration:none;">Data Jabatan</a></li>
        <li><a href="<?=base_url();?>barang/listbarang" style="text-decoration:none;">Data Barang</a></li>
        <li><a href="<?=base_url();?>jenis_barang/listjenisbarang" style="text-decoration:none;">Data Jenis Barang</a></li>
        <li><a href="<?=base_url();?>supplier/listsupplier" style="text-decoration:none;">Data Supplier</a></li>
        
      </ul>
    </li>
    <li>
    <label for='drop-1' class='toggle'>Transaksi <span>&#9776;</span></label>
      <a href='#'>Transaksi &#9776;</a>
       <ul>
     	 <li><a href="<?=base_url();?>pembelian/listpembelian" style="text-decoration:none;">Data Pembelian</a></li>
         <li><a href="<?=base_url();?>penjualan/listpenjualan" style="text-decoration:none;">Data Penjualan</a></li>
       </ul>
    <li/>      
    
    <li><a href="<?=base_url();?>pembelian/dtp" style="text-decoration:none;">Report</a></li>
	<li><a href="<?=base_url();?>auth/logout">Log Out</a></li>
  </ul>
</nav>
</div>
</div>