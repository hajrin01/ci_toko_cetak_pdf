<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	$user_login = $this->session->userdata();
 ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<title>CI_TOKO</title>
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({dateFormat : "yy-dd-mm"});
	$( "#datepicker1" ).datepicker({dateFormat : "yy-dd-mm"});
	
  } );
  </script>
 </head>
 <body>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
  
   <center>
<?php 
	if ($user_login['tipe']== "1"){
		$this->load->view('template/view_menu');
	}else{
		$this->load->view('template/view_menu_user');
	}
	
?>

</center>

<?php $this->load->view($content);?>
</body>