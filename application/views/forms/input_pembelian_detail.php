
<div id="container2">
	<center><h2 style="font-family:'Comic Sans MS', cursive">Input Pembelian Detail</h2></center>
    <div id="body" style="text-align: center;">

    <center><div style="color: red"><?= validation_errors(); ?></div></center>
    <form action="<?=base_url()?>pembelian/inputDetail/<?= $id_header; ?>" method="POST">
    <table width="27%" cellspacing="3" cellpadding="8" style="margin: 0 auto;">
	<tr>
        <td style="text-align: right;">Nama Barang</td>
        <td>:</td>
        <td style="text-align: left;">
            <select id="kode_barang" class="form-control" name="kode_barang" value="<?= set_value('kode_barang');?>" />
                <?php foreach($data_barang as $data) { ?>
                <option value="<?= $data->kode_barang; ?>"><?= $data->nama_barang; ?></option>
                <?php }?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="45%" style="text-align: right;">Qty</td>
        <td width="3%">:</td>
        <td width="52%" style="text-align: left;">
            <input type="text" id="qty" class="form-control" name="qty" value="<?= set_value('qty');?>" />
        </td>
    </tr>
    <tr>
        <td width="45%" style="text-align: right;">Harga Barang</td>
        <td width="3%">:</td>
        <td width="52%" style="text-align: left;">
            <input type="text" id="harga" class="form-control" name="harga" value="<?= set_value('harga');?>" />
        </td>
    </tr>
    </tr>
       <table width="20%" align="center">
	   <tr>
        <td>
        <input type="submit" value="Proses" class="btn btn-info" name="simpan" style="background-color:#06F" /> 
        
        <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset" style="background-color:#F00">        
        </td>
	</tr>
    </table>
</table>
</form><br/>
<table width="100%" border="1" cellspacing="0" cellpadding="3">
	<tr align="center" style="color:#FFF" bgcolor="#333333">
		<td width="3%">No</td>
		<td width="15%">Kode Barang</td>
		<td width="22%">Nama Barang</td>
		<td width="20%">Qty</td>
		<td width="24%">Harga</td>
		<td width="24%">Jumlah</td>
    </tr>
    <?php
    
        $no = 0;
        $total_hitung = 0;
        foreach ($data_pembelian_detail as $data) {
            $no++;
    ?>
	<tr align="center">
		<td><?= $no; ?></td>
		<td><?= $data->kode_barang; ?></td>
		<td><?= $data->nama_barang; ?></td>
		<td><?= $data->qty; ?></td>
		<td align="right">Rp. <?= number_format($data->harga); ?> ,-</td>
		<td  align="right">Rp. <?= number_format($data->jumlah); ?> ,-</td>
    </tr>
    <?php
        // hitung total
        $total_hitung += $data->jumlah;
        } 
    ?>
    <tr align="center">
		<td colspan="5" align="right"><b>TOTAL</b></td>
		<td  align="right">Rp. <b><?= number_format($total_hitung); ?></b></td>
    </tr>
    </table>
    <br/>
        <a href="<?= base_url(); ?>pembelian/index">
            <input type="button" value="Kembali Ke Menu Sebelumnya" class="btn btn-info" name="kembali" />
        </a>
</div>
	
</div>