

  <center><h2 style="font-family:'Comic Sans MS', cursive">Edit Barang</h2></center>
 
 <?php
foreach ($detail_barang as $data) {
	$kode_barang  = $data->kode_barang;
	$nama_barang  = $data->nama_barang;
	$harga_barang  = $data->harga_barang;
	$kode_jenis  = $data->kode_jenis;
	$stok  = $data->stok;
}

?>
 <center><div style="color: red"><?= validation_errors(); ?></div></center>
<form action="<?=base_url()?>barang/editbarang/<?= $kode_barang; ?>" method="POST">
<table width="29%" cellspacing="3" cellpadding="8" align="center">
  <tr>
    <br />  
    <td>Kode Barang</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_barang" id="kode_barang" class="form-control" maxlength="10" value ="<?=$kode_barang;?>" readonly /></td>
  </tr>
  <tr>
    <td>Nama Barang</td>
    <td>:</td>
    <td>
     <input type="text" name="nama_barang" id="nama_barang" class="form-control" value ="<?=$nama_barang;?>" ></td>
  </tr>
  <tr>
    <td>Harga</td>
    <td>:</td>
    <td>
     <input type="text" name="harga_barang" id="harga_barang" class="form-control" value ="<?=$harga_barang;?>" /></td>
  </tr>
  
  <tr>
  <td>Nama Jenis</td>
    <td>:</td>
    <td><select name="kode_jenis" id="kode_jenis" class="form-control" value ="<?=$kode_jenis ;?>" >
    <?php foreach($data_jenis_barang as $data) {
		$select_jenis = ($data->kode_jenis == $kode_jenis) ? 'selected' : '';
		?>
       <option value="<?= $data->kode_jenis;?>" <?=$select_jenis; ?>>
       <?= $data->nama_jenis; ?></option>
       
      
      <?php }?>
      
      </select>
      </td>
  </tr>

  <tr>
    <td>stok</td>
    <td>:</td>
    <td>
     <input type="text" name="stok" id="stok" class="form-control" value ="<?=$stok ;?>" /></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" class="btn btn-info" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset" style="background-color:#F00">
      <br></br>
      <a href="<?=base_url();?>barang/listbarang"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a></td>
  </tr>
</table>
</form>
</table>
