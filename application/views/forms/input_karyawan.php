
  <center><h2 style="font-family:'Comic Sans MS', cursive" >Input Karyawan</h2></center>

  <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>karyawan/inputkaryawan" method="POST" enctype="multipart/form-data">
<table width="39%" border="0" cellspacing="3" cellpadding="8" align="center">
  <tr>
    <br/>  
    <td>Nik</td>
    <td>:</td>
    <td>
      <input type="text" class="form-control" name="nik" id="nik" value="<?=set_value('nik');?>" maxlength="10"></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>
      <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="<?=set_value('nama_lengkap');?>" maxlength="50"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td> <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="<?=set_value('tempat_lahir');?>" maxlength="50"></td>
    </td>
  </tr>
<tr>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tgl_lahir" ).datepicker({dateFormat : "yy-dd-mm"});
  
  
  } );
  </script>
    <td width="112" height="66">Tanggal Lahir</td>
    <td width="39">:</td>
    <td width="179">
     <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir" 
     value="<?=set_value('tgl_lahir');?>"></td>
  </tr>



  <tr>
    <td height="35">Jenis Kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
       <option value="L">Laki laki</option>
       <option value="P">Perempuan</option>
      
    </select>
    </td>
  </tr>
  
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" class="form-control" name="telp" id="telp" value="<?=set_value('telp');?>"></td>
    </td>
  </tr>
 
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" class="form-control" id="alamat" cols="45" rows="3"><?=set_value('alamat');?></textarea></td>
  </tr>
 <tr>
    <td height="35">Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan" class="form-control">
    <?php foreach($data_jabatan as $data) {?>
       <option value="<?= $data->kode_jabatan;?>">
       <?= $data->nama_jabatan; ?></option>
       
      
      <?php }?>
      
    </select>
    </td>
  </tr>
   <tr>
    <td style="text-align:left;">Upload Foto</td>
    <td>:</td>
    <td style="text-align:left;">
    <input type="file" name="image" id="image"/>
    </td>
  </tr>
  <tr>
    <td height="64">&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan"class="btn btn-info" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset" style="background-color:#F00">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a></td>
  </tr>
</table>
</table>
</form>

