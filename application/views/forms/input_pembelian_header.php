
<div id="container2">
	<center><h2 style="font-family:'Comic Sans MS', cursive">Input Pembelian</h2></center>
    <div id="body" style="text-align: center;">
        <center><div style="color: red"><?= validation_errors(); ?></div></center>
    <form action="<?=base_url()?>pembelian/input/?>" method="POST" enctype="multipart/form-data">
    <table width="35%" border="0" cellspacing="0" cellpadding="8" style="margin: 0 auto;">
    <tr>
        <td width="45%" style="text-align: right;">Nomor Transaksi</td>
        <td width="3%">:</td>
        <td width="52%" style="text-align: left;">
            <input type="text" id="no_transaksi" class="form-control" name="no_transaksi" value="<?= set_value('no_transaksi');?>" />
        </td>
    </tr>
	<tr>
        <td style="text-align: right;">Nama Supplier</td>
        <td>:</td>
        <td style="text-align: left;">
            <select id="kode_supplier" class="form-control" name="kode_supplier" value="<?= set_value('kode_supplier');?>" />
                <?php foreach($data_supplier as $data) { ?>
                <option value="<?= $data->kode_supplier; ?>"><?= $data->nama_supplier; ?></option>
                <?php }?>
            </select>
        </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td style="text-align: left;">
        <input type="submit" value="simpan" class="btn btn-info" name="simpan" style="background-color:#06F" /> 
        <input type="reset" value="reset" class="btn btn-info" name="reset" style="background-color:#F00" />
        <br/>
        <br/>
        <a href="<?= base_url(); ?>pembelian/index">
            <input type="button" value="Kembali Ke Menu Sebelumnya" name="kembali"
            class="btn btn-info" style="background-color:#0FF" />
        </a>
        </td>
	</tr>
</table>
</form>
</div>
	
</div>