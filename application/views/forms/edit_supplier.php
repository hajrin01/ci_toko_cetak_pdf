
<center ><h2 style="font-family:'Comic Sans MS', cursive">Edit Supplier</h2></center>

<?php
foreach ($detail_supplier as $data) {
	$kode_supplier = $data->kode_supplier;
	$nama_supplier  = $data->nama_supplier;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	
}

?>
  <center><div style="color: red"><?= validation_errors(); ?></div></center>
<form action="<?=base_url()?>supplier/editsupplier/<?= $kode_supplier; ?>" method="POST">
<table width="38%" cellspacing="3" cellpadding="8" align="center">
  <tr>
    <br />  
    <td>Kode Supplier</td>
    <td>:</td>
    <td>
      <input type="text" class="form-control" name="kode_supplier" id="kode_supplier" maxlength="10" value ="<?=$kode_supplier;?>" value="<?= set_value('kode_supplier');?>" readonly /></td>
  </tr>
  <tr>
    <td>Nama Supplier</td>
    <td>:</td>
    <td>
     <input type="text" class="form-control" name="nama_supplier" id="nama_supplier" value ="<?=$nama_supplier;?>" value="<?= set_value('nama_supplier');?>" /></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" class="form-control" id="alamat" cols="45" rows="3" value="<?= set_value('alamat');?>" /><?=$alamat;?></textarea></td>
  </tr>
  
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" class="form-control" name="telp" id="telp" value ="<?=$telp;?>" value="<?= set_value('telp');?>" /></td>
    </td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="simpan" id="simpan" class="btn btn-info" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset"
       style="background-color:#F00">
      <br></br>

      <a href="<?=base_url();?>supplier/listsupplier"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a></td>
  </tr>
</table>
</form>
</body>