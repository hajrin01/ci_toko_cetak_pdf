
<center><h3>Laporan Data Pembelian Dari Tanggal</h3></center>
  <center><h4><?=$tgl_awal;?> s/d <?=$tgl_akhir;?></h4></center>
</br>
<table width="84%" align="left">
<td align="right"><a href="<?=base_url();?>pembelian/cetak/<?= $tgl_awal; ?>/<?= $tgl_akhir ;?>"><input type="submit" name="cetak" value="Cetak PDF"></a></td>        
</table>
<table width="68%" border="1" bordercolor="#CCCCCC" cellspacing="0" cellpadding="7" align="center">
<tr align="center" style="color:#FFF" bgcolor="#000000" >
    <td>No</td>
    <td>ID Pembelian</td>
    <td>Nomor Transaksi</td>
   	<td>Tanggal</td>
    <td>Total Barang</td>
    <td>Total Qty</td>
    <td>Jumlah Nominal Pembelian</td>
    

 </tr>
  <?php
  $no = 0;
   $total_keseluruhan = 0;

    foreach ($data_pembelian_detail as $data) {
		$no++;

?>
<tr align="center">
   
    <td><?=$no;?></td>
    <td><?= $data->id_pembelian_h; ?></td>
	  <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tanggal; ?></td>
    <td><?= $data->total_barang; ?></td>
    <td><?= $data->total_qty; ?></td>
    <td>RP. <?= number_format($data->total_pembelian); ?></td>
</tr>
<?php 
		//menghitung total
		$total_keseluruhan+= $data->total_pembelian;
	}
?>
</table>

<table width="68%" cellspacing="0" cellpadding="7" align="center">
<tr bgcolor="#00FFFF" align="right">
<td>Total Keseluruhan Pembelian &emsp; Rp.<?=number_format($total_keseluruhan); ?></td>
 </tr>
 </table>
  
