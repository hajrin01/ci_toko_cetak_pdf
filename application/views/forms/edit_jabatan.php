
  <center><h2 style="font-family:'Comic Sans MS', cursive">Edit Jabatan</h2></center>
 
 <?php
foreach ($detail_jabatan as $data) {
	$kode_jabatan  = $data->kode_jabatan;
	$nama_jabatan  = $data->nama_jabatan;
	$keterangan  = $data->keterangan;
}
?>

<center><div style="color: red"><?= validation_errors(); ?></div></center>
<form action="<?=base_url()?>jabatan/editjabatan/<?= $kode_jabatan; ?>" method="POST">
<table width="30%" cellspacing="3" cellpadding="8" align="center">
  <tr>
    <br />  
    <td>Kode Jabatan</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_jabatan" id="kode_jabatan" class="form-control" maxlength="5" value ="<?=$kode_jabatan;?>"  value="<?= set_value('kode_jabatan');?>" readonly /></td>
  </tr>
  <tr>
    <td>Nama Jabatan</td>
    <td>:</td>
    <td>
     <input type="text" name="nama_jabatan" id="nama_jabatan" class="form-control" value ="<?=$nama_jabatan;?>" value="<?= set_value('nama_jabatan');?>" /></td>
  </tr>
  
  <tr>
    <td height="35">Keterangan</td>
    <td>:</td>
    <td><select name="keterangan" id="keterangan" class="form-control" value ="<?=$keterangan;?>" value="<?= set_value('keterangan');?>" />
       <option value="Operasional">Operasional</option>
	   <option value="Manager">Manager</option>
      
    </select>
    </td>
  </tr>
  
  
  
 
  
 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" class="btn btn-info" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset" style="background-color:#F00">
      <br></br>
      <a href="<?=base_url();?>jabatan/listjabatan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a></td>
  </tr>
</table>
</table>
</form>
</body>